<div class="row">
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="form-group">
			<label for="form_ime">Ime</label>
			<input type="text" class="form-control input-sm" name="name" id="form_ime" placeholder="Ime" data-validation="required" data-validation-error-msg="Ime je potrebno">
		</div>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="form-group">
			<label for="form_prezime">Prezime</label>
			<input type="text" class="form-control input-sm" name="surname" id="form_prezime" placeholder="Prezime" data-validation="required" data-validation-error-msg="Prezime je potrebno">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<label for="form_adresa">Adresa</label>								
	</div>
	<div class="col-xs-6">
		<div class="form-group">
			<input type="text" class="form-control input-sm" name="country" id="form_adresa_drzava" placeholder="Država">
		</div>
	</div>
	<div class="col-xs-6">
		<div class="form-group">
			<input type="text" class="form-control input-sm" name="city" id="form_adresa_grad" placeholder="Grad">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-6">
		<div class="form-group">
			<input type="text" class="form-control input-sm" name="street" id="form_adresa_ulica" placeholder="Ulica">
		</div>
	</div>
	<div class="col-xs-6">
		<div class="form-group">
			<input type="text" class="form-control input-sm" name="street_num" id="form_adresa_kucniBr" placeholder="Kućni broj">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="form-group">
			<label for="form_telefon">Broj telefona</label>
			<input type="text" class="form-control input-sm" name="telephone" id="form_telefon" placeholder="Telefon">
		</div>
	</div>
	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
		<div class="form-group">
			<label for="form_mobitel">Broj mobitela</label>
			<input type="text" class="form-control input-sm" name="mobile" id="form_mobitel" placeholder="Mobitel">
		</div>
	</div>
</div>
<div class="form-group">
	<label for="form_email">Email adresa</label>
	<input type="text" class="form-control input-sm" name="email" id="form_email" placeholder="Email" data-validation="email" data-validation-optional="true">
</div>
<div class="form-group">
	<label for="form_birthDate">Datum rođenja</label>
	<div class='input-group _form_firthDate__picker'>
		<input type="text" class="form-control input-sm" name="birthday" id="form_birthDate" placeholder="Datum rođenja" data-validation="date" data-validation-format="dd.mm.yyyy." data-validation-optional="true">
		<span class="input-group-addon _datePickerIcon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
	</div>
</div>
<div class="form-group">
	<label for="form_komentar">Komentar</label>
	<textarea class="form-control input-sm" name="comment" id="form_komentar" placeholder="Komentar"></textarea>
</div>

<script>
	$('._form_firthDate__picker').datetimepicker({
		format: 'DD.MM.YYYY.',
		defaultDate: '01.01.1990.'
	});
</script>
<?php
namespace php\app\core;

include_once 'database/Db.php';
include_once  dirname(__FILE__) . '/../models/User.php';

use php\app\core\database\Db;
use php\app\models\User;

class Helpers {
	protected $db;

	public function __construct() {
		$this->db = Db::getInstance();
	}

	public static function setFlashMessage($key, $message) {
		$_SESSION['FLASH_MSGS'][$key] = $message;
	}

	public static function getFlashMessage($key) {
		if (!empty($_SESSION['FLASH_MSGS']) && !empty($_SESSION['FLASH_MSGS'][$key])) {
			$message = $_SESSION['FLASH_MSGS'][$key];
			unset($_SESSION['FLASH_MSGS'][$key]);
			return $message;
		}
		return false;
	}

	public static function authenticate($userKeyValue, $passwordKeyValue) {
		$thisModel = new Helpers();
		$userModel = new User();
		foreach($userKeyValue as $key => $value) {
			$queryArr[] = $key . "='" . $value . "'";
		}
		foreach($passwordKeyValue as $key => $value) {
			$queryArr[] = $key . "='" . hash('sha256', $value) . "'";
		}
		$queryStr = 'WHERE ' . implode(' AND ', $queryArr);
		$result = $thisModel->db->selectQuery($userModel, '*', $queryStr);

		if (count($result) != 0) {
			foreach($result[0] as $key => $value) {
				$_SESSION['AUTH'][$key] = $value;
			}
			return true;
		}
		return false;
	}

	public static function logout() {
		session_start();
		unset($_SESSION["AUTH"]);
		return true;
	}

	public static function isLoggedIn() {
		return (isset($_SESSION["AUTH"]));
	}

	public static function authUser() {
		$thisModel = new Helpers();
		$userModel = new User();
		$authUser = $thisModel->db->findById($userModel, $_SESSION['AUTH']['id']);
		return $authUser;
	}

	public static function register($user, $uniqueKeyValue) {
		$thisModel = new Helpers();
		foreach($uniqueKeyValue as $key => $value) {
			$queryArr[] = $key . "='" . $value . "'";
		}
		$queryStr = 'WHERE ' . implode(' AND ', $queryArr);
		$result = $thisModel->db->selectQuery($user, '*', $queryStr);
		if (count($result) >= 0) {

		} 
		else {

		}
	}
}

?>
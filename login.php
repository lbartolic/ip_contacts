<?php

session_start();

?>

<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<?php include('partials/head/css.php'); ?>
		<link href="resources/assets/css/login.css" rel="stylesheet" type="text/css">
		<?php include('partials/head/js.php'); ?>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row _login-row">
				<div class="_login-holder">
					<form method="POST" action="php/app/set.php">
						<h1>Adresar</h1><h2>Prijava</h2>
						<input type="text" class="form-control" name="username" placeholder="Korisničko ime" autofocus="">
						<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Lozinka">
						<button class="btn btn btn-primary btn-block" type="submit">Prijavi se</button>
						<input type="hidden" name="_action" value="user_login">
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

<script>
	
</script>
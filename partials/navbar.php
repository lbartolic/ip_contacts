<?php
	include_once dirname(__FILE__) . '/../php/app/core/Helpers.php';
	use php\app\core\Helpers;
?>

<nav class="navbar navbar-default _navbar-main" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href=".">Adresar</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li class="main-nav__item" id="nav__contacts"><a href="contacts.php">Moji kontakti</a></li>
				<li class="main-nav__item" id="nav__map"><a href="map.php">Mapa kontakata</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i><?= Helpers::authUser()->first_name . ' ' . Helpers::authUser()->last_name;?></a>
					<ul class="dropdown-menu _dropdown-smaller _dropdown-darker">
						<form method="POST" action="php/app/set.php">
							<input type="hidden" name="_action" value="user_logout">
        					<li><a href="#" class="_aFormSubmit"><i class="fa fa-sign-out"></i>Odjava</a></li>
    					</form>
					</ul>
				</li>
				<div style="display: block;">
				
				</div>
			</ul>
		</div>
	</div>
</nav>
<?php
namespace php\app\models;

include_once './Db.php';
use php\app\Db;
	
class Test2 {
	protected $table = 'test';

	public function __construct() {
		$this->db = new Db();
	}

	public function test() {
		$resultArray = [];
		$testvar = 345;
		if ($statement = $this->db->get()->prepare("SELECT var FROM $this->table WHERE var = ?")) {
			$statement->bind_param('i', $testvar);
			$statement->execute();
			$statement->bind_result($var);

			while($statement->fetch()) {
				$resultArray[] = $var;
			}

			return $resultArray;
		}
		throw new \Exception($this->db->get()->error);
	}
}

?>
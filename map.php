<?php
include_once 'php/app/models/Contact.php';
include_once 'php/app/core/Helpers.php';

use php\app\models\Contact;
use php\app\core\Helpers;

session_start();
if (Helpers::isLoggedIn()) {
	$contacts = Contact::selectQuery('*', "WHERE user_id='" . Helpers::authUser()->id . "'");
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Adresar – Mapa kontakata</title>
		<?php include('partials/head/css.php'); ?>
		<?php include('partials/head/js.php'); ?>
	</head>
	<body style="overflow: hidden;">
		<?php include 'partials/navbar.php';?>
		<div class="container-fluid _full-map-cont">
			<div class="row">
				<div id="contacts_map">

				</div>
			</div>
		</div>
		<?php include('partials/loadingSpinner.php');?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2SzyBSaVDiM_h_wg62hE2U1oDhKFCpXQ"></script>
	</body>
</html>

<script>
	setNavbar('nav__map');

	$('#contacts_map').css({
		'height': $(window).height(),
		'width': $(window).width()
	});
	$(window).resize(function() {
		$('#contacts_map').css({
			'height': $(window).height(),
			'width': $(window).width()
		});
	});

	function shuffleArray(o) {
		for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
		return o;
	}
	function makeContactsMap() {
		var map = new google.maps.Map(document.getElementById('contacts_map'), {
        	zoom: 6
        });
		var contacts = <?= json_encode($contacts);?>;
		var bounds = new google.maps.LatLngBounds();
		var markerIcons = ['green-dot.png', 'yellow-dot.png', 'purple-dot.png', 'red-dot.png', 'blue-dot.png']
		console.log(contacts);
		for (var i = 0 ; i < contacts.length ; i++) {
			var contact = contacts[i];
			if (contact.country != null && contact.city != null && contact.street != null && contact.street_num != null) {
				var address = contact.country + "," + contact.city;
				address = address + "," + contact.street + " " + contact.streetNum;

		        var geocoder = new google.maps.Geocoder();
			    (function(i) { geocoder.geocode({'address': address}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						shuffleArray(markerIcons);
						map.setCenter(results[0].geometry.location);

						var contactCountry, contactCity, contactStreet, contactStretNum;
						(contacts[i].country == null) ? contactCountry = "-" : contactCountry = contacts[i].country;
						(contacts[i].city == null) ? contactCity = "-" : contactCity = contacts[i].city;
						(contacts[i].street == null) ? contactStreet = "-" : contactStreet = contacts[i].street;
						(contacts[i].street_num == null) ? contactStretNum = "-" : contactStretNum = contacts[i].street_num;
						var contentString = "<h4>" + contacts[i].name + " " + contacts[i].surname + "</h4>" +
							"<h5>" + contactCountry + ", " + contactCity + ", " + contactStreet + " " + contactStretNum + "</h5>";
						var infowindow = new google.maps.InfoWindow({
							content: contentString
						});

						var marker = new google.maps.Marker({
							map: map,
							position: results[0].geometry.location,
							icon: 'http://maps.google.com/mapfiles/ms/icons/' + markerIcons[0]
						});
						marker.addListener('click', function() {
							infowindow.open(map, marker);
						});
						bounds.extend(marker.getPosition());
						map.setCenter(bounds.getCenter());
						map.fitBounds(bounds);
					} else {
						console.log('asd');
					}
			    }); })(i);
			}
		}
	}
	makeContactsMap();
</script>

<?php
} else {
	header('Location: index.php');
}
?>
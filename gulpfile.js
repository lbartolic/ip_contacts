var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.styles([
        "css.css"
    ], 'resources/production/css/app.css', 'resources/assets/css');
	mix.scripts([
	    "source.js"
    ], 'resources/production/js/app.js', 'resources/assets/js');

	mix.styles([
	    "./resources/assets/css/animate.css",
		"./resources/assets/css/bootstrap.min.css",
		"./resources/assets/css/bootstrap-datetimepicker.min.css",
		"./resources/assets/css/formvalidatorTheme.min.css",
		"./resources/assets/css/select2.min.css",
		"./resources/plugins/font-awesome-4.5.0/css/font-awesome.min.css",
		"./resources/plugins/sweetalert/css/sweetalert.css",
		"./resources/plugins/nanoscroller/nanoscroller.css",
		"./resources/plugins/lobibox/css/lobibox.min.css",
		"./resources/assets/css/css.css"
	], 'resources/production/css/all.css');
	mix.scripts([
	    "./resources/assets/js/jquery-1.10.2.js",
		"./resources/assets/js/jquery-ui.js",
		"./resources/assets/js/bootstrap.min.js",
		"./resources/assets/js/moment.js",
		"./resources/assets/js/bootstrap-datetimepicker.min.js",
		"./resources/assets/js/select2.full.min.js",
		"./resources/assets/js/chartjs.min.js",
		"./resources/assets/js/jquery.form-validator.min.js",
		"./resources/assets/js/underscore.min.js",
		"./resources/assets/js/handlebars-v4.0.5.js",
		"./resources/plugins/handlebars-intl/handlebars-intl.min.js",
		"./resources/plugins/handlebars-intl/hr.js",
		"./resources/plugins/sweetalert/js/sweetalert.min.js",
		"./resources/plugins/nanoscroller/nanoscroller.min.js",
		"./resources/plugins/lobibox/js/lobibox.min.js",
		"./resources/assets/js/source.js"
	], 'resources/production/js/all.js');
});

<?php
namespace php\app\models;

include_once dirname(__FILE__) . '/../core/Model.php';
include_once dirname(__FILE__) . '/../core/interfaces/ModelInterface.php';

use php\app\core\Model;
use php\app\core\interfaces\ModelInterface;
	
class User extends Model implements ModelInterface {
	protected $table = 'users';
	protected $fillable = [
		'username',
		'password',
		'first_name',
		'last_name',
		'created_at',
		'updated_at'
	];
	protected $redirect = '';
}

?>
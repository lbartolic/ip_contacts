HandlebarsIntl.registerWith(Handlebars);
Handlebars.registerHelper("handleNullValue", function(val) {
    if (val === undefined || val === null || val === "") {
    	return "–";
    }
    return val;
});

function setupDeleteConfEvent() {
	$(document).ready(function() {
	    $('._delete-conf').off().on('click', function(){
	        var $form = $(this).closest("form");
	        swal({   
	            title: "Upozorenje",
	            text: "Jeste li sigurni da ovo želite obrisati?",
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Da, obriši",
	            cancelButtonText: "Odustani",
	            closeOnConfirm: false 
	        }, 
	        function() {   
	            $form.submit();
	        });
	    });
    });
}
setupDeleteConfEvent();

function setupLinkFormSubmit() {
	$(document).ready(function() {
	    $('._aFormSubmit').click(function() {
	    	$(this).closest('form').submit();
	    });
    });
}
setupLinkFormSubmit();


function showNotification(notifTitle, notifMsg, notifType, notifDelayInd) {
    var title = true,
        msg = '',
        type = 'success',
        delayIndicator = true;
            
    if (notifTitle !== undefined && notifTitle !== null) title = notifTitle;
    if (notifMsg !== undefined && notifMsg !== null) msg = notifMsg;
    if (notifType !== undefined && notifType !== null) type = notifType;
    if (notifDelayInd !== undefined && notifDelayInd !== null) delayIndicator = notifDelayInd;
    
    Lobibox.notify(type, {
        title: title,
        size: 'normal',
        delayIndicator: delayIndicator,
        delay: 5000,
        rounded: false,
        position: 'center bottom',
        msg: msg,
        sound: false,
        showClass: 'fadeInUp',
        hideClass: 'fadeOutDown'
    });
}

function setNavbar(itemId) {
	$('.main-nav__item').removeClass('active');
	$('#' + itemId).addClass('active');
}

/* ------------------------------ CONTACTS DATA MANIPULATION ------------------------------ */

function makePhoneBook(data, start) {
	if (start === null || start === undefined) start = 0;
	if (data === null || data === undefined) data = JSON.parse(localStorage.getItem("contactsJSON"));

	var numOfItems = 12;
	var to = start + numOfItems;
	if (to > data.length) to = data.length;
	var templateScript = document.getElementById('contacts-handlebars-t').innerHTML;
	var template = Handlebars.compile(templateScript);
	var context = {'contacts': data.slice(start, to)};
	var intlData = {
	    "locales": "hr-HR"
	};
	var compiledHTML = template(context, {data: {intl: intlData}});
	document.getElementById('contacts-holder').innerHTML = compiledHTML;
	$("._pb-item").each(function(key) {
		$(this).delay(key * 30).fadeIn();
	});

	if (start == 0) {
		$('#contacts-pag__prev, #contacts-pag__first').attr('disabled', 'disabled');
	}
	else {
		$('#contacts-pag__prev, #contacts-pag__first').removeAttr('disabled');
	}
	if (to >= data.length) {
		$('#contacts-pag__next, #contacts-pag__last').attr('disabled', 'disabled');
	}
	else {
		$('#contacts-pag__next, #contacts-pag__last').removeAttr('disabled');
	}


	$('#contacts-pag__first').off().click(function() {
		makePhoneBook(data, 0);
	});
	$('#contacts-pag__last').off().click(function() {
		makePhoneBook(data, data.length - numOfItems);
	});
	$('#contacts-pag__prev').off().click(function() {
		var prevStart;
		(start - numOfItems < 0) ? prevStart = 0 : prevStart = start - numOfItems;
		makePhoneBook(data, prevStart);
	});
	$('#contacts-pag__next').off().click(function() {
		makePhoneBook(data, to);
	});

	var paginFromNum;
	(data.length != 0) ? paginFromNum = start + 1 : paginFromNum = 0;
	$('#pagin-info__fromNum').html(paginFromNum);
	$('#pagin-info__toNum').html(to);
	$('#pagin-info__totalNum').html(data.length);

	$('._pb-expand-btn').off().click(function() {
		showPhoneBookItem(this);
	});
	setupDropdownActions();
}

function setupDropdownActions() {
	setupDeleteConfEvent();

	$('._new-contact-from').off().click(function(e) {
		hideActivePhoneBookItem();
		e.preventDefault();
		var id = $(this).attr('data-id');
		$.get("php/app/set.php", {_action: 'get_contact', id: id})
		.done(function(data) {
			$.each(JSON.parse(data), function(key, value) {
				if (key == 'birthday' && value != null) value = moment(value).format('DD.MM.YYYY.');
				$('#new-contact-form input[name="'+key+'"]').val(value);
				$('#new-contact-form textarea[name="'+key+'"]').val(value);
			});
			$('#new-contact-well').get(0).scrollIntoView();
		}, "json")
		.fail(function() { });
	});

	$('._edit-contact').off().click(function(e) {
		hideActivePhoneBookItem();
		e.preventDefault();
		var id = $(this).attr('data-id');
		$.get("php/app/set.php", {_action: 'get_contact', id: id})
		.done(function(data) {
			var jsonData = JSON.parse(data);
			$.each(jsonData, function(key, value) {
				if (key == 'birthday' && value != null) value = moment(value).format('DD.MM.YYYY.');
				$('#updateContactModal input[name="'+key+'"]').val(value);
				$('#updateContactModal textarea[name="'+key+'"]').val(value);
			});
			$('#update-c__contact').html(jsonData.name + " " + jsonData.surname);
			$('#updateContactModal').modal('show');
		}, "json")
		.fail(function() { });
	});
}

function searchPhoneBook(searchTerm) {
	var searchTerm = $('#contacts-search__input').val();
	var data = JSON.parse(localStorage.getItem("contactsJSON"));
	data = _.filter(data, function(contact) {
		return ((contact.name.toUpperCase()).indexOf(searchTerm.toUpperCase()) > -1 || (contact.surname.toUpperCase()).indexOf(searchTerm.toUpperCase()) > -1 || (contact.name.toUpperCase() + " " + contact.surname.toUpperCase()).indexOf(searchTerm.toUpperCase()) > -1);
	});
	makePhoneBook(data);
}

function filterPhoneBook(el) {
	$('._dropdown-pb-filter li').removeClass('active');
	$(el).parent().addClass('active');
	var filterSelected = el.getAttribute('data-filter');
	var data = JSON.parse(localStorage.getItem("contactsJSON"));
	switch(filterSelected) {
		case 'asc':
			data = _.sortBy(data, function(contact) { return contact.surname.toUpperCase(); });
			break;
		case 'desc':
			data = _.sortBy(data, function(contact) { return contact.surname.toUpperCase(); });
			data.reverse();
			break;
		case 'oldest':
			data = _.sortBy(data, function(contact) { return parseInt(contact.id) });
			break;
		case 'newest':
			data = _.sortBy(data, function(contact) { return parseInt(contact.id) });
			data.reverse();
			break;
	}
	localStorage.setItem("contactsJSON", JSON.stringify(data));
	makePhoneBook();
}

/* ------------------------------ CONTACTS DATA MANIPULATION ------------------------------ */

function centerPhoneBookActiveItem() {
	var $clicked = $('._pb-item-active');
	if ($clicked.parent().offset() !== undefined) {
		var $info = $('#info');
		var w = $clicked.width();
		var h = $clicked.height();
		var top = 0 + $(window).scrollTop();
		var left = $(window).width()/2 - w/2 - $clicked.parent().offset().left;

		$clicked.css({
			'left': left,
			'top': top
		});
		$('#info').css({
			'top': $clicked.offset().top + $clicked.height() + 10,
			'left': $clicked.offset().left + $clicked.width()/2 - $('#info ._pb-item-more').innerWidth()/2
		});
	}
}

function showPhoneBookItem(elem) {
	$(elem).hide();
	var $clicked = $(elem).closest('._pb-item');
	$('._fade').fadeIn();

	var clickedTop = $clicked.position().top;
	var clickedLeft = $clicked.position().left - $clicked.width()/2;

	$('._pb-item').removeClass('_pb-item-active');
	$clicked.addClass('_pb-item-active');

	var w = $clicked.outerWidth();
	var h = $clicked.height();
	var top = 0 + $(window).scrollTop();
	var left = $(document).width()/2 - w/2 - $clicked.parent().offset().left;

	$('._pb-item-active').css({
		'left': clickedLeft,
		'top': clickedTop
	});
	$('._pb-item-active ._info-basic').hide();
	$('._pb-item-active ._info-expanded').show();

	$clicked.animate({
		'top': top,
		'left': left
	}, 350, function() {
		var info = '<div id="info"></div>';
		$('body').append(info);
		var itemMore = $clicked.find('._pb-item-more').clone();
		$('#info').html(itemMore);
		$('#info ._pb-item-more').css({
			'display': 'block'
		});
		$('#info').css({
			'opacity': 0,
			'top': $clicked.offset().top + $clicked.height() - 20,
			'left': $clicked.offset().left + $clicked.width()/2 - $('#info .container').innerWidth()/2
		});
		$('#info').animate({
			'opacity': 1,
			'top': $clicked.offset().top + $clicked.height() + 10
		}, 300, function() {
			showLoadingSpinner('#info ._pb-panel-map');
			var $pbPanelMap = $('#info ._pb-panel-map');
			$pbPanelMap.append('<div id="pb-item-map"></div>');

			var contactCountry = $pbPanelMap.attr("data-country");
			var contactCity = $pbPanelMap.attr("data-city");
			var contactStreet = $pbPanelMap.attr("data-street");
			var contactStreetNum = $pbPanelMap.attr("data-streetNum");
			setPhoneBookItemMap('pb-item-map', contactCountry, contactCity, contactStreet, contactStreetNum, function(status, precision) {
				hideLoadingSpinner('#info ._pb-panel-map');
				if (status == 'loaded') {
					$('#pb-item-map').animate({'opacity': 1}, 200);
					if (precision == 1) $('#info .alert-warning').fadeIn('fast');
					if (precision == 2) $('#info .alert-success').fadeIn('fast');
				}
				if (status == 'error') {
					$('#info .alert-danger').fadeIn('fast');
					$pbPanelMap.find('#pb-item-map').remove();
				}
			});
		});
	});
}

function hideActivePhoneBookItem() {
	$('._pb-item-active ._info-expanded').hide();
	$('._pb-item-active ._info-basic').show();
	$('._pb-item-active ._pb-expand-btn').show();
	$('._pb-item-active').removeClass('_pb-item-active');
	$('._fade').fadeOut();
	$('#info').fadeOut(function() {
		$('#info').remove();
	});
}

function setPhoneBookItemMap(mapDivId, country, city, street, streetNum, callback) {
	if (country != "" && city != "") {
		var precision = 1;
		var address = country + "," + city;
		if (street != "" && streetNum != "") {
			address = address + "," + street + " " + streetNum;
			precision = 2;
		}
		var mapLoaded = false;
        var geocoder = new google.maps.Geocoder();

        map = new google.maps.Map(document.getElementById(mapDivId), {
        	zoom: 11
        });
	    geocoder.geocode({'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});
				callback('loaded', precision);
			} else {
				console.log('asd');
				callback('error');
			}
	    });
	}
	else {
		callback('error');
	}
}

function showLoadingSpinner(selector) {
	var loadingSpinner = $('body').find('._loadingSpinner__holder').clone();
	$(selector).append(loadingSpinner);
	$(selector).find('._loadingSpinner__holder').show();
}

function hideLoadingSpinner(selector) {
	$(selector).find('._loadingSpinner__holder').remove();
}
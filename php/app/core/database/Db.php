<?php 
namespace php\app\core\database;
	
class Db extends \PDO {
	private static $connection;
	protected $db_name = 'ip_contacts';
	protected $db_user = 'root';
	protected $db_pass = 'password';
	protected $db_host = 'localhost';

	public function __construct() {
        try {
            parent::__construct('mysql:host=' . $this->db_host . ';dbname=' . $this->db_name . ';charset=utf8', $this->db_user, $this->db_pass);
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch(\PDOException $e) {
		    echo $e->getMessage();
        }
    }

	public static function getInstance() {
		if (self::$connection == null) {
			self::$connection = new self();
		} 
		return self::$connection;
   }



   public function selectQuery($obj, $columns, $query) {
		$table = $obj->getTable();

		if ($statement = self::prepare("SELECT " . $columns . " FROM $table " . $query)) {
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_CLASS, get_class($obj)); 

			return $statement->fetchAll();
		}
		throw new \Exception(self::error);
	}

	public function findById($obj, $id) {
		$table = $obj->getTable();

		if ($statement = self::prepare("SELECT * FROM $table WHERE id = '$id'")) {
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_CLASS, get_class($obj)); 

			return $statement->fetch();
		}
		throw new \Exception(self::error);
	}

	public function save($obj, $user = null) {
		$table = $obj->getTable();
		$fillable = $obj->getFillable();

		$values = get_object_vars($obj);
		$fields = $fillable;
		$keys = array_keys($values);
		if ($user != null) $keys[] = $obj->getUserColumn();
		$fieldsStr = implode(',', $fillable);
		$keysStr = implode(',', $keys);

		foreach($values as $key => $value) {
			(empty($values[$key])) ? $values[$key] = NULL : $values[$key];
			if (!in_array($key, $fields)) {
				unset($values[$key]);
			}
		}
		$values = array_values($values);
		if ($user != null) $values[] = $user->id;
		$prepareVal = array('?');
		$prepareVals = array_pad($prepareVal, count($keys), '?');
		$prepareValsStr = implode(',', $prepareVals);

		if ($statement = self::prepare("INSERT INTO $table ($keysStr) VALUES ($prepareValsStr)")) {
			$statement->execute($values);

			return $obj;
		}
		throw new \Exception(self::error);
	}

	public function update($obj, $id) {
		$table = $obj->getTable();
		$fillable = $obj->getFillable();

		$values = get_object_vars($obj);
		$fields = $fillable;
		foreach($values as $key => $value) {
			if (!in_array($key, $fields)) {
				unset($values[$key]);
			}
		}

		$updateArr = array();
		foreach($values as $key => $value) {
			$updateArr[] = $key . '=' . ':' . $key;
		}
		$updateStr = implode(',', $updateArr);

		if ($statement = self::prepare("UPDATE $table SET $updateStr WHERE id = '$id'")) {
			foreach($values as $key => $value) {
				$statement->bindValue(':' . $key, $value);
			}
			$statement->execute();

			return $statement->rowCount();
		}
		throw new \Exception(self::error);
	}

	public function updateMultiple() {

	}

	public function delete($obj, $id) {
		$table = $obj->getTable();

		if ($statement = self::prepare("DELETE FROM $table WHERE id = '$id'")) {
			$statement->execute();

			return $statement->rowCount();
		}
		throw new \Exception($this->get()->error);
	}

	public function deleteMultiple() {

	}
}

?>
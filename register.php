<?php

include_once 'php/app/core/Helpers.php';

use php\app\core\Helpers;

session_start();

?>

<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<?php include('partials/head/css.php'); ?>
		<link href="resources/assets/css/login.css" rel="stylesheet" type="text/css">
		<?php include('partials/head/js.php'); ?>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row _login-row">
				<div class="_login-holder">
					<form method="POST" action="php/app/set.php">
						<h1>Adresar</h1><h2>Registracija</h2>
						<input type="text" class="form-control" name="first_name" placeholder="Ime" autofocus="">
						<input type="text" class="form-control" name="last_name" placeholder="Prezime">
						<input type="text" class="form-control" name="username" placeholder="Korisničko ime">
						<input type="password" class="form-control" name="password" placeholder="Lozinka">
						<button class="btn btn btn-primary btn-block" type="submit">Registriraj se</button>
						<input type="hidden" name="_action" value="user_register">
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

<script>
	<?php if ($flashMessage = Helpers::getFlashMessage('user_register')): ?>
		showNotification("<?= $flashMessage['title'];?>", "<?= $flashMessage['msg'];?>", "<?= $flashMessage['status'];?>");
	<?php endif; ?>
</script>
<?php
namespace php\app\core;

include_once 'database/Db.php';
include_once 'Helpers.php';

use php\app\core\database\Db;
use php\app\core\Helpers;
	
abstract class Model {
	protected $db;

	public function __construct() {
		$this->db = Db::getInstance();
	}

	public function getTable() {
		return $this->table;
	}

	public function getFillable() {
		return $this->fillable;
	}

	public function getUserColumn() {
		return $this->belongsToCol;
	}

	public function modelSelectQuery($cols, $q) {
		$result = $this->db->selectQuery($this, $cols, $q);
		return $result;
	}
	public static function selectQuery($cols = "*", $q = "") {
		$thisModel = new static;
		$result = $thisModel->modelSelectQuery($cols, $q);
		return $result;
	}

	public function save($user = null) {
		$result = $this->db->save($this, $user);
		return $result;
	}

	public function update() {
		$updateId = $this->id;
		$result = $this->db->update($this, $updateId);
		return $result;
	}

	public function modelDelete($id) {
		//$deleteId = $this->id;
		$result = $this->db->delete($this, $id);
		return $result;
	}
	public static function delete($id) {
		$thisModel = new static;
		$thisModel->modelDelete($id);
		return $thisModel;
	}

	public function modelFindById($id) {
		$result = $this->db->findById($this, $id);
		return $result;
	}
	public static function findById($id) {
		$thisModel = new static;
		$result = $thisModel->modelFindById($id);
		return $result;
	}
}

?>
<div class="modal pulse animated" id="updateContactModal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title"><b id="update-c__contact"></b></h4>
			</div>
			<form action="php/app/set.php" method="POST" role="form" id="new-contact-form">
				<div class="modal-body">
					<?php include dirname(__FILE__) . '/../form.php';?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
					<button type="submit" class="btn btn-primary">Spremi promjene</button>
				</div>
				<input type="hidden" name="id" value="">
				<input type="hidden" name="_action" value="save_contact">
			</form>
		</div>
	</div>
</div>
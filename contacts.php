<?php
include_once 'php/app/models/Contact.php';
include_once 'php/app/core/Helpers.php';

use php\app\models\Contact;
use php\app\core\Helpers;

session_start();
if (Helpers::isLoggedIn()) {
	$contacts = Contact::selectQuery('*', "WHERE user_id='" . Helpers::authUser()->id . "'");
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Adresar – Kontakti</title>
		<?php include('partials/head/css.php'); ?>
		<?php include('partials/head/js.php'); ?>
	</head>
	<body>
		<?php include 'partials/contacts/modals/updateContact.php';?>
		<?php include 'partials/navbar.php';?>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="well clearfix _content-card">
						<div class="_phone-book-topbar clearfix">
							<!--<div class="_pb-topbar-left">
								<span><b></b> kontakata</span>
							</div>-->
							<div class="row">
							<div class="_pb-topbar-left col-sm-6 col-xs-12">
								<div class="input-group">
									<input type="text" class="form-control" id="contacts-search__input" placeholder="Pretraži kontakte...">
									<span class="input-group-btn">
										<button class="btn btn-default" id="contacts-search__btn" type="button"><i class="fa fa-search _fa-reset _fa-full"></i></button>
									</span>
								</div>
							</div>
							<div class="_pb-topbar-right col-sm-6 col-xs-12">
								<div class="form-inline clearfix pull-right">
    								<div class="form-group">
										<span id="pagin-info" class="_paginationInfo"><b id="pagin-info__fromNum">1</b><b> – </b><b id="pagin-info__toNum">4</b> od <b id="pagin-info__totalNum">20</b></span>
										<div class="btn-group" role="group">
											<button type="button" id="contacts-pag__first" class="btn btn-default"><i class="fa fa-angle-double-left _fa-reset _fa-bigger"></i></button>
											<button type="button" id="contacts-pag__prev" class="btn btn-default"><i class="fa fa-angle-left _fa-reset _fa-bigger"></i></button>
											<button type="button" id="contacts-pag__next" class="btn btn-default"><i class="fa fa-angle-right _fa-reset _fa-bigger"></i></button>
											<button type="button" id="contacts-pag__last" class="btn btn-default"><i class="fa fa-angle-double-right _fa-reset _fa-bigger"></i></button>
										</div>
										<div class="btn-group _tooltip" data-toggle="tooltip" data-placement="top" title="Filtriranje i sortiranje" role="group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter _fa-reset _fa-full"></i></button>
											<ul class="dropdown-menu _dropdown-smaller pull-right _dropdown-pb-filter">
												<li><a href="#" data-filter="oldest"><i class="fa fa-sort-numeric-asc"></i>Najstarije dodani</a></li>
												<li class="active"><a href="#" data-filter="newest"><i class="fa fa-sort-numeric-desc"></i>Najnovije dodani</a></li>
												<li role="separator" class="divider"></li>
												<li><a href="#" data-filter="asc"><i class="fa fa-sort-alpha-asc"></i>Od A do Ž</a></li>
												<li><a href="#" data-filter="desc"><i class="fa fa-sort-alpha-desc"></i>Od Ž do A</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							</div>
						</div>
						<script id="contacts-handlebars-t" type="text/x-handlebars-template">
							{{#each contacts}}
								<div class="_pb-item" style="display: none;">
									<div class="_pb-i-top">
										<h2 class="_text-ellipsis">{{name}} {{surname}}</h2>
									</div>
									<div class="_pb-i-content">
										<div class="_pb-i-info-holder clearfix _info-basic">
											<div class="_pb-i-info">
												<span class="_info-title"><i class="fa fa-mobile"></i>Broj mobitela</span>
												<span class="_info-value _text-break">
													{{handleNullValue mobile}}
												</span>
											</div>
											<div class="_pb-i-info">
												<span class="_info-title"><i class="fa fa-map-marker"></i>Lokacija</span>
												<span class="_info-value _text-break">
													{{handleNullValue country}}
												</span>
											</div>
										</div>
										<div class="_pb-i-info-holder clearfix _info-expanded" style="display: none;">
											<div class="row">
												<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
													<div class="_pb-i-info">
														<span class="_info-title"><i class="fa fa-mobile"></i>Broj mobitela</span>
														<span class="_info-value _text-break">
															{{mobile}}
														</span>
													</div>
													<div class="_pb-i-info">
														<span class="_info-title"><i class="fa fa-map-marker"></i>Lokacija</span>
														<span class="_info-value _text-break">
															{{country}}, {{city}}
														</span>
													</div>
													<div class="_pb-i-info">
														<span class="_info-title"><i class="fa fa-home"></i>Adresa</span>
														<span class="_info-value _text-break">
															{{street}} {{street_num}}
														</span>
													</div>
												</div>
												<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
													<div class="_pb-i-info">
														<span class="_info-title"><i class="fa fa-phone"></i>Broj telefona</span>
														<span class="_info-value _text-break">
															{{telephone}}
														</span>
													</div>
													<div class="_pb-i-info">
														<span class="_info-title"><i class="fa fa-pencil"></i>Email</span>
														<span class="_info-value _text-break">
															{{email}}
														</span>
													</div>
													<div class="_pb-i-info">
														<span class="_info-title"><i class="fa fa-pencil"></i>Rođendan</span>
														<span class="_info-value _text-break">
															{{formatDate birthday day="numeric" month="long" year="numeric"}}
														</span>
													</div>
												</div>
											</div>
											<div class="row _marginTop10">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<div class="_pb-i-info">
														<span class="_info-title"><i class="fa fa-pencil"></i>Komentar</span>
														<span class="_info-value _text-break">
															{{comment}}
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="_pb-i-actions clearfix">
											<button class="btn btn-xs btn-primary _pb-expand-btn">
												<i class="fa fa-expand _fa-reset _fa-full"></i>
											</button>
											<div class="btn-group pull-right _tooltip" data-toggle="tooltip" data-placement="top" title="Više">
												<button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fa fa-ellipsis-h _fa-reset _fa-full"></i>
												</button>
												<ul class="dropdown-menu _dropdown-smaller">
													<li><a href="#" class="_edit-contact" data-id="{{id}}"><i class="fa fa-edit"></i>Uredi</a></li>
													<li><a href="#" class="_new-contact-from" data-id="{{id}}"><i class="fa fa-copy"></i>Novi od ovog</a></li>
													<li role="separator" class="divider"></li>
													<form method="POST" action="php/app/set.php">
														<li><a href="#" class="_delete-conf"><i class="fa fa-remove"></i>Obriši</a></li>
														<input type="hidden" name="id" value="{{id}}">
														<input type="hidden" name="_action" value="delete_contact">
													</form>
												</ul>
											</div>
										</div>
									</div>
									<div class="container _pb-item-more" style="display: none;">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<h3 class="_pb-panel-h">Lokacija na mapi</h3>
														<div class="alert alert-warning _alert-sm" role="alert" style="display: none;">
															Lokacija je određena približno: <b>{{country}}, {{city}}</b>.
														</div>
														<div class="alert alert-success _alert-sm" role="alert" style="display: none;">
															Prikaz točne lokacije: <b>{{country}}, {{city}}, {{street}} {{street_num}}</b>.
														</div>
														<div class="alert alert-danger _alert-sm" role="alert" style="display: none;">
															Lokaciju <b>nije moguće prikazati</b> zbog nedostatka ili neispravnosti podataka.
														</div>
														<div class="_pb-panel-map"
															data-country="{{country}}"
															data-city="{{city}}"
															data-street="{{street}}"
															data-streetNum="{{street_num}}">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							{{/each}}
						</script>
						<div class="_phone-book" id="contacts-holder">
							<div class="_pb-item"></div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="well clearfix _new-contact-well _content-card" id="new-contact-well">
						<form action="php/app/set.php" method="POST" role="form" id="new-contact-form">
							<legend>Novi zapis</legend>
							<?php include 'partials/contacts/form.php';?>
							<button type="submit" class="btn btn-primary pull-right">Spremi</button>
							<button type="reset" class="btn btn-default pull-right _marginRight6">Poništi</button>
							<input type="hidden" name="_action" value="save_contact">
						</form>
					</div>
				</div>
			</div>
			
		</div>
		<div class="_fade"></div>
		<?php include('partials/loadingSpinner.php');?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2SzyBSaVDiM_h_wg62hE2U1oDhKFCpXQ"></script>
	</body>
</html>

<script>
	var contactsJSON = _.sortBy(<?= json_encode($contacts);?>, function(contact) { return parseInt(contact.id) });
	localStorage.setItem("contactsJSON", JSON.stringify(contactsJSON.reverse()));

	setNavbar('nav__contacts');
	$(document).ready(function() {
		$.validate();
		makePhoneBook();

		$('#contacts-search__input').keyup(function() { searchPhoneBook(); });
		$('._dropdown-pb-filter li a').click(function() { filterPhoneBook(this); });

		$('._tooltip').tooltip();
		$('._fade').click(function() {
			hideActivePhoneBookItem();
		});
	});
	$(window).resize(function() {
		centerPhoneBookActiveItem();
	});

	<?php if ($flashMessage = Helpers::getFlashMessage('save_contact')): ?>
		showNotification("<?= $flashMessage['title'];?>", "<?= $flashMessage['msg'];?>", "<?= $flashMessage['status'];?>");
	<?php endif; ?>

	<?php if ($flashMessage = Helpers::getFlashMessage('delete_contact')): ?>
		showNotification("<?= $flashMessage['title'];?>", "<?= $flashMessage['msg'];?>", "<?= $flashMessage['status'];?>");
	<?php endif; ?>
</script>

<?php
} else {
	header('Location: index.php');
}
?>
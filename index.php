<?php
include_once 'php/app/models/Contact.php';
include_once 'php/app/core/Helpers.php';

use php\app\models\Contact;
use php\app\core\Helpers;

session_start();

(Helpers::isLoggedIn()) ? header('Location: contacts.php') : header('Location: login.php');

?>
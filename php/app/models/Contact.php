<?php
namespace php\app\models;

include_once dirname(__FILE__) . '/../core/Model.php';
include_once dirname(__FILE__) . '/../core/interfaces/ModelInterface.php';

use php\app\core\Model;
use php\app\core\interfaces\ModelInterface;
	
class Contact extends Model implements ModelInterface {
	protected $table = 'contacts';
	protected $belongsToCol = 'user_id';
	protected $fillable = [
		'name',
		'surname',
		'country',
		'city',
		'street',
		'street_num',
		'telephone',
		'mobile',
		'comment',
		'email',
		'birthday'
	];
	protected $redirect = '';
}

?>
<?php
include_once 'models/Contact.php';
include_once 'models/User.php';
include_once 'core/Helpers.php';

use php\app\models\Contact;
use php\app\models\User;
use php\app\core\Helpers;

$ROOT_PATH = '../../';
session_start();

/* REQUESTS FOR LOGGED IN USERS */
if (Helpers::isLoggedIn()) {
	$authUser = Helpers::authUser();

	if (isset($_POST['_action'])) {
		switch($_POST['_action']) {
			case 'save_contact':
				if (!empty($_POST['name']) && !empty($_POST['surname'])) {
					(isset($_POST['id'])) ? $contact = Contact::findById($_POST['id']) : $contact = new Contact();

					$contact->name = $_POST['name'];
					$contact->surname = $_POST['surname'];
					$contact->country = $_POST['country'];
					$contact->city = $_POST['city'];
					$contact->street = $_POST['street'];
					$contact->street_num = $_POST['street_num'];
					$contact->telephone = $_POST['telephone'];
					$contact->mobile = $_POST['mobile'];
					$contact->comment = $_POST['comment'];
					$contact->email = $_POST['email'];
					if (!empty($_POST['birthday'])) {
						$contactBirthday = DateTime::createFromFormat('d.m.Y.', $_POST['birthday']);
						$contact->birthday = $contactBirthday->format('Y-m-d 00:00:00');
					}
					else $contact->birthday = NULL;

					if (isset($_POST['id'])) {
						if ($contact->user_id == $authUser->id) {
							$contact->update();
							$flashMsg = array('title' => 'Uspješno spremljeno', 'msg' => 'Kontakt je uspješno izmjenjen.', 'status' => 'success');
						}
					}
					else {
						$contact->save($authUser);
						$flashMsg = array('title' => 'Uspješno spremljeno', 'msg' => 'Novi kontakt je dodan na popis.', 'status' => 'success');
					}
				}
				else {
					$flashMsg = array('title' => 'Pogreška', 'msg' => 'Neko od polja za unos nije pravilno uneseno.', 'status' => 'error');
				}
				Helpers::setFlashMessage('save_contact', $flashMsg);
				header('Location: ' . $ROOT_PATH . 'contacts.php');
				break;

			case 'delete_contact':
				$id = $_POST['id'];
				$contact = Contact::findById($id);
				if ($contact->user_id == $authUser->id) {
					Contact::delete($id);
					Helpers::setFlashMessage('delete_contact', array(
						'title' => 'Uspješno izbrisano', 'msg' => 'Kontakt je izbrisan.', 'status' => 'success'
					));
				}
				header('Location: ' . $ROOT_PATH . 'contacts.php');
				break;

			case 'user_logout':
				Helpers::logout();
				header('Location: ' . $ROOT_PATH . 'index.php');
				break;
		}
	}

	if (isset($_GET['_action'])) {
		switch($_GET['_action']) {
			case 'get_contact':
				$id = $_GET['id'];
				$contact = Contact::findById($id);
				if ($contact->user_id == $authUser->id) echo json_encode($contact);
				break;
		}
	}
}
/* REQUESTS FOR ALL USERS (AUTHENTICATED & GUESTS) */
else {
	if (isset($_POST['_action'])) {
		switch($_POST['_action']) {
			case 'user_login':
				$username = $_POST['username'];
				$password = $_POST['password'];
				Helpers::authenticate(array('username' => $username), array('password' => $password));
				header('Location: ' . $ROOT_PATH . 'index.php');
				break;

			case 'user_register':
				$user = new User();
				$user->first_name = $_POST['first_name'];
				$user->last_name = $_POST['last_name'];
				$user->username = $_POST['username'];
				$user->password = hash('sha256', $_POST['password']);

				if (!empty($user->first_name) && !empty($user->last_name) && !empty($user->username) && !empty($user->password)) {
					if (count(User::selectQuery("*", "WHERE username='" . $user->username . "'")) == 0) {
						$user->save();
						Helpers::authenticate(array('username' => $user->username), array('password' => $_POST['password']));
						header('Location: ' . $ROOT_PATH . 'index.php');
					}
					else {
						Helpers::setFlashMessage('user_register', array(
							'title' => 'Pogreška', 'msg' => 'Korisnik s ovim korisničkim imenom već postoji.', 'status' => 'error'
						));
						header('Location: ' . $ROOT_PATH . 'register.php');
					}
				}
				else {
					Helpers::setFlashMessage('user_register', array(
						'title' => 'Pogreška', 'msg' => 'Svi podaci moraju biti uneseni.', 'status' => 'error'
					));
					header('Location: ' . $ROOT_PATH . 'register.php');
				}
				break;
		}
	}
	/* NO REQUEST, NOT AUTHENTICATED */
	else {
		header('Location: ' . $ROOT_PATH . 'index.php');
	}
}

?>